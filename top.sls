base:
  '*':
    - common
  'salt':
    - master
  'node*':
    - postgres.server
  'node[1-3]':
    - postgres.repmgr
    - consul
  'node4':
    - postgres.repmgr.ssh
    - barman

